package com.google.test;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class HomeTest {
	
	@Parameters({"value","turl"})
	@Test
	public void TC1(String value,String url)
	{
		System.out.println("HomeTest TC1 "+value);
		System.out.println("HomeTest TC1  url "+url);
	} 
	@Test
	public void TC2()
	{
		System.out.println("HomeTest TC2");
	}

	@Test
	public void TC3()
	{
		System.out.println("HomeTest TC3");
	}
}
